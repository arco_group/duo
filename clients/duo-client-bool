#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

import sys
import logging
import Ice

from client_base import ClientBase, ClientException
import DUO


class BoolClient(ClientBase):
    def add_options(self):
        group = self.arg_parser.add_mutually_exclusive_group(required=True)
        group.add_argument(
            '--set', choices=('True', 'False'), help="set object's value")
        group.add_argument(
            '--get', action='store_true', help="get object's value")

    def do_set(self):
        if self.args.no_checked_cast:
            proxy = DUO.IBool.WPrx.uncheckedCast(self.proxy)
        else:
            proxy = DUO.IBool.WPrx.checkedCast(self.proxy)

        if proxy is None:
            raise ClientException("given proxy is not an IBool.W")

        value = self.args.set.lower() == 'true'
        proxy.set(value, Ice.Identity())

    def do_get(self):
        proxy = DUO.IBool.RPrx.checkedCast(self.proxy)
        if proxy is None:
            raise ClientException("given proxy is not an IBool.R")

        value = proxy.get()
        logging.info("Value: {0}".format(value))


if __name__ == "__main__":
    BoolClient().main(sys.argv, "../servers/icestorm-client.cfg")
