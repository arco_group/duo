# -*- mode: python; coding: utf-8 -*-

import logging
import argparse

import Ice
import IceStorm
from gi.repository import GObject


logging.basicConfig(level=logging.DEBUG,
                    format='DUMMY|%(levelname)s: %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')


def loadSlice(name):
    args = '-I{0} -I. -I../slice --all {1}'
    Ice.loadSlice(args.format(Ice.getSliceDir(), name))


loadSlice('dummy.ice')
loadSlice('../slice/DUO.ice')

import DUO

# Used on derived classes
import DummyDUO


class DummyException(Exception):
    def __init__(self, msg):
        self.message = msg


# FIXME: inherit comodity.pattern.Observable
class Subject(object):
    observers = []
    state = None

    def set_state(self, val):
        self.state = val
        self.notify()

    def get_state(self):
        return self.state

    def attach(self, ob):
        logging.info("attach: '{0}'".format(ob))
        self.observers.append(ob)

    def detach(self, ob):
        if ob in self.observers:
            logging.info("detach: '{0}'".format(ob))
            self.observers.remove(ob)

    def notify(self):
        def idle_call():
            for ob in self.observers:
                try:
                    logging.info("updating '{0}'".format(ob))
                    ob.update()
                except Exception, e:
                    logging.error("unexpected behaviour: {0}".format(e))
                    logging.error("removing observer '{0}'".format(ob))
                    self.detach(ob)

        GObject.idle_add(idle_call)


class Active_I(Subject):
    def __init__(self):
        self.publisher = None

    def getObserver(self, current=None):
        return self.publisher

    def setObserver(self, publisher, current=None):
        self.publisher = self.cast_proxy(publisher)
        self.reactive = Reactive(self, self.publisher)

    def ice_ping(self, current=None):
        logging.info("-> ping()")
        self.notify()


class Dummy_R(Subject):
    def get(self, current=None):
        state = self.get_state()
        logging.info(" -> get(), return: {0}".format(state))
        return state


class Dummy_W(Subject):
    def set(self, val, oid, current):
        ic = current.adapter.getCommunicator()
        oid = ic.identityToString(oid)
        logging.info("-> set({0}, '{1}')".format(val, oid))

        self.set_state(val)


class Reactive(object):
    def __init__(self, model, publisher):
        self.model = model
        self.publisher = publisher
        self.model.attach(self)

    def update(self):
        state = self.model.get_state()
        self.publisher.set(state, Ice.Identity())
        logging.info('<- observer.set({0})'.format(state))

    def __repr__(self):
        return "Reactive({0})".format(self.model.ice_id())


class DUOCaster(object):
    def cast_proxy(self, proxy):
        module_name = self.ice_id()[2:].replace("::", ".")
        exec "proxy = {0}Prx.uncheckedCast(proxy)".format(module_name)
        return proxy


class MetaDummy(type):
    def __new__(metacls, name):

        def init(obj, val=None):
            obj.state = val

        duo_ifaces = {'R': Dummy_R, 'W': Dummy_W, 'A': Active_I}
        bases = [duo_ifaces[i] for i in name.split('.')[1]]
        bases.append(DUOCaster)

        if len(name.split('.')[1]) == 1:
            module = "DUO"
        else:
            module = "DummyDUO"

        base = None
        try:
            exec "base = {0}.{1}".format(module, name)
        except AttributeError:
            raise DummyException("there is no {0} module!".format(name))

        bases.append(base)
        return type.__new__(metacls, name, tuple(bases),
                            {'__init__': init, 'base': base})


class Dummy(Ice.Application):
    def run(self, args):
        self.build_parser()
        self.add_options()

        try:
            self.args = self.arg_parser.parse_args(args[1:])
            self.process_args()
        except DummyException as e:
            logging.error("ERROR: {0}".format(e.message))
            return 2
        except SystemExit as e:
            self.communicator().shutdown()
            return e.code

        self.wait()

    def process_args(self):
        self.build_endpoint_string()
        self.create_adapter()
        self.create_servant()
        self.register_servant()

        self.post_setup_servant()
        self.create_gui()

    def build_endpoint_string(self):
        self.endp = "tcp"
        if self.args.port is not None:
            self.endp += " -p {0}".format(self.args.port)

        if self.args.host is not None:
            self.endp += " -h {0}".format(self.args.host)

    def create_adapter(self):
        try:
            ic = self.communicator()
            self.adapter = ic.createObjectAdapterWithEndpoints(
                "DummyAdapter", self.endp)
        except Ice.SocketException, e:
            raise DummyException(str(e))

        self.adapter.activate()

    def create_servant(self):
        self.servant = self.do_create_servant()

    def do_create_servant(self):
        """ To implement on subclass """
        raise NotImplementedError

    def post_setup_servant(self):
        if 'A' in self.args.interface:
            self.setup_servant_as_active()

        if (self.args.topic_suscribe is not None
            or self.args.active_suscribe is not None):
            self.setup_servant_as_observer()

    def setup_servant_as_active(self):
        topic = self.get_servant_topic()
        publisher = topic.getPublisher()
#        publisher = self.servant.cast_proxy(publisher)
        self.servant.setObserver(publisher)

    def get_servant_topic(self):
        topic_mgr = self.get_topic_manager()
        topic_name = Ice.generateUUID()

        try:
            topic = topic_mgr.create(topic_name)
        except IceStorm.TopicExists:
            topic = topic_mgr.retrieve(topic_name)

        logging.info("topic proxy: '{0}'".format(topic))
        return topic

    def get_topic_manager(self):
        ic = self.communicator()
        topic_mgr = ic.propertyToProxy("ISTopicAsFacet.TopicManager.Proxy")
        topic_mgr = IceStorm.TopicManagerPrx.checkedCast(topic_mgr)
        return topic_mgr

    def setup_servant_as_observer(self):
        topic = self._get_topic_to_suscribe()
        try:
            topic.subscribeAndGetPublisher({}, self.proxy)
        except IceStorm.AlreadySubscribed:
            pass

        logging.info("suscribed to '{0}'".format(topic))

    def _get_topic_to_suscribe(self):
        ic = self.communicator()
        if self.args.topic_suscribe:
            topic = ic.stringToProxy(self.args.topic_suscribe)
        else:
            active = ic.stringToProxy(self.args.active_suscribe)
            active = DUO.Active.RPrx.uncheckedCast(active)
            publisher = active.getObserver()
            topic = publisher.ice_facet('topic')

        topic = IceStorm.TopicPrx.checkedCast(topic)
        return topic

    def register_servant(self):
        if not self.args.identity:
            self.proxy = self.adapter.addWithUUID(self.servant)
        else:
            oid = self.communicator().stringToIdentity(self.args.identity)
            self.proxy = self.adapter.add(self.servant, oid)

        logging.info("dummy proxy: '{0}'".format(self.proxy))

    def get_proxy_as_string(self):
        return self.communicator().proxyToString(self.proxy)

    def get_observer_as_string(self):
        if not self.is_duo_active():
            raise DummyException("This dummy is not active, no observer available")

        observer = self.servant.getObserver()
        return self.communicator().proxyToString(observer)

    def create_gui(self):
        if self.args.nogui:
            return

        self.gui = self.do_create_gui()
        self.gui.show()

    def do_create_gui(self):
        """ To implement on subclass """
        raise NotImplementedError

    def wait(self):
        self.callbackOnInterrupt()
        ic = self.communicator()

        self.loop = GObject.MainLoop()
        self.loop.run()

        ic.shutdown()

    def interruptCallback(self, args):
        GObject.idle_add(self.loop.quit)

    def build_parser(self):
        self.arg_parser = argparse.ArgumentParser(description='Dummy')
        self.arg_parser.add_argument(
            '--id', dest='identity', help='identity of servant')
        self.arg_parser.add_argument(
            '--port', type=int, help='port of object adapter')
        self.arg_parser.add_argument(
            '--host', help='ip where to listen')

        self.arg_parser.add_argument(
            '--topic-suscribe',
            help='topic on which suscribe servant')
        self.arg_parser.add_argument(
            '--active-suscribe',
            help='publisher of an istaf topic, on which suscribe servant')

        self.arg_parser.add_argument(
            '--no-gui', dest='nogui', action='store_true',
            help="don't use graphical user interface")

    def add_options(self):
        """ To implement on subclass """
        pass

    def is_duo_active(self):
        return "A" in self.args.interface

    def set_state(self, state):
        self.servant.set_state(state)

    def get_state(self):
        return self.servant.get_state()

    def attach(self, observer):
        self.servant.attach(observer)
