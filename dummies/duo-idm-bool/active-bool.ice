// -*- mode: c++; coding: utf-8 -*-

#include <duo/duo_idm.ice>

module Dummy {
    interface ActiveBool extends DUO::IDM::IBool::W, DUO::IDM::Active::W {};
};
