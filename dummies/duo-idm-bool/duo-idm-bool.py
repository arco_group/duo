#!/usr/bin/env python
# -*- mode: python; coding: utf-8 -*-

import sys
import os
import Ice
import json
import argparse
import thing
import webbrowser
from threading import Event
from flask import Flask, render_template, request, Response

pwd = os.path.abspath(os.path.dirname(__file__))
slice_path = os.path.join(pwd, "active-bool.ice")
if not os.path.exists(slice_path):
    slice_path = "/usr/share/duo/dummy/active-bool.ice"

Ice.loadSlice("{} -I/usr/share/slice --all".format(slice_path))
Ice.loadSlice("/usr/share/slice/PropertyService/PropertyService.ice -I/usr/share/slice --all")
import Dummy
import PropertyService
import DUO
import IDM


class IBoolWI(Dummy.ActiveBool):
    def __init__(self, args, router):
        self.args = args
        self.state = args.state == "True"
        self.state_changes = Event()
        self.address = args.address
        self.router = router
        self.observer = None

        if router is not None:
            self.ic = router.ice_getCommunicator()

        if args.observer is not None:
            addr = self.stringToAddress(args.observer)
            self.setObserver(addr)

    def stop(self):
        # wake all waiting threads
        self.state_changes.set()

    def get_state_when_changed(self):
        self.state_changes.wait()
        self.state_changes.clear()
        return self.state

    def set(self, state, source, current=None):
        print(" * Set state to {}".format(state))
        self.state = state
        self.state_changes.set()

        if self.observer is None:
            return

        print(" * Notify observer: '{}'".format(self.observer))
        self.observer.set(state, "")

    def setObserver(self, observer, current=None):
        if 'producer' not in self.args.tags or self.router is None:
            return

        is_null = True
        for c in observer:
            if c != '\x00':
                is_null = False
                break

        if is_null:
            self.observer = None
            print(" * Removing observer")

        else:
            observer = self.addressToString(observer)
            self.observer = self.router.ice_identity(self.ic.stringToIdentity(observer))
            self.observer = self.observer.ice_encodingVersion(Ice.Encoding_1_0)
            self.observer = self.observer.ice_oneway()
            self.observer = DUO.IDM.IBool.WPrx.uncheckedCast(self.observer)
            print(" * Set observer to '{}'".format(self.observer))

    def addressToString(self, address):
        return "".join("{:02X}".format(ord(c)) for c in address)

    def stringToAddress(self, addr):
        return "".join(chr(int(x, 16)) for x in addr.split(":"))


class WebServer:
    def __init__(self):
        self.ic = Ice.initialize(sys.argv[1:])

    def add_self(fn):
        def deco(*args, **kwargs):
            return fn(WebServer.instance, *args, **kwargs)
        deco.__name__ = fn.__name__
        return deco

    def run(self):
        WebServer.instance = self

        self.parse_args(sys.argv)
        self.create_proxies()
        self.setup_servant()
        self.update_properties()
        self.announce_on_router()

        if self.args.open_browser:
            webbrowser.open(self.server_url())

        self.event_loop()

    def event_loop(self):
        self.should_stop = False

        self.app.run(threaded=True, host="0.0.0.0", port=self.args.web_server_port)
        self.ic.destroy()
        self.servant.stop()
        self.should_stop = True
        print("\rBye!")

    def server_url(self):
        return 'http://0.0.0.0:{}/'.format(self.args.web_server_port)

    def announce_on_router(self):
        if self.router is None:
            return

        self.router.adv(self.ic.proxyToString(self.servant_proxy))

    def update_properties(self):
        if self.ps is None:
            return

        t_tags = thing.from_(self.args.tags)
        t_name = thing.from_(self.args.name)
        t_icon = thing.from_(self.args.icon)

        print(" * Setting properties on PropertyService")
        self.ps.set("{} | tags".format(self.args.address), t_tags)
        self.ps.set("{} | name".format(self.args.address), t_name)
        self.ps.set("{} | icon".format(self.args.address), t_icon)

    def create_proxies(self):
        self.router = self.ic.propertyToProxy("IDM.Router.Proxy")
        if self.router is None:
            print(" * WARN: IDM.Router.Proxy not defined")
        else:
            print(" * IDM Router: '{}'".format(self.router))
            self.router = IDM.NeighborDiscovery.ListenerPrx.uncheckedCast(self.router)

        self.ps = self.ic.propertyToProxy("PropertyService.Proxy")
        if self.ps is None:
            print(" * WARN: PropertyService.Proxy not defined")
        else:
            print(" * Property Server: '{}'".format(self.ps))
            self.ps = PropertyService.PropertyServerPrx.uncheckedCast(self.ps)

    def setup_servant(self):
        adapter = self.ic.createObjectAdapterWithEndpoints("Adapter", "tcp")
        adapter.activate()

        self.servant = IBoolWI(self.args, self.router)
        oid = self.ic.stringToIdentity(self.args.address)
        self.servant_proxy = adapter.add(self.servant, oid)
        print(" * Proxy ready: '{}'".format(self.servant_proxy))

    def parse_args(self, args):
        parser = argparse.ArgumentParser()

        def parse_addr(v):
            return v.replace(":", "").upper()

        parser.add_argument(
            "-a", dest="address", default="AA:02", help="IDM Address", type=parse_addr)
        parser.add_argument(
            "-s", dest="state", default=False, choices=["True", "False"], help="Initial state")
        parser.add_argument(
            '-n', dest="name", default="Dummy", help="Dummy's name")
        parser.add_argument(
            '-i', dest="icon", default="lightbulb-o", help="Dummy's icon (Font Awesome names)")
        parser.add_argument(
            '-o', dest="observer", default=None, help="Set observer address", type=parse_addr)
        parser.add_argument(
            '--open-browser', action="store_true", help="Open URL on a web browser")
        tags = ['consumer', 'producer']
        parser.add_argument(
            '--tags', nargs='*', choices=tags, default=tags, help="Set property 'tags'")
        parser.add_argument(
            '--port', dest="web_server_port", default=5000, type=int, help="Web server port")

        self.args, _ = parser.parse_known_args()

    app = Flask(__name__)

    @app.route("/")
    @add_self
    def index(self):
        return render_template("index.html")

    @app.route("/status")
    @add_self
    def get_status(self):
        observer = self.args.observer
        if self.servant.observer:
            observer = Ice.identityToString(self.servant.observer.ice_getIdentity())

        return json.dumps({
            'state': self.servant.state,
            'address': self.servant.address,
            'icon': self.args.icon,
            'observer': observer,
        })

    @app.route("/set")
    @add_self
    def set_state(self):
        new_state = request.args['state'] == "true"
        self.servant.set(new_state, "")
        return ""

    @app.route("/updates")
    @add_self
    def send_updates(self):
        def wait_for_changes():
            while True:
                state = self.servant.get_state_when_changed()
                if self.should_stop:
                    return
                data = {"state": state}
                yield "data: {}\n\n".format(json.dumps(data))

        return Response(wait_for_changes(), mimetype="text/event-stream")


if __name__ == "__main__":
    WebServer().run()
